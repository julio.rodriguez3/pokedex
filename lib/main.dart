import 'package:flutter/material.dart';

import 'src/ui/pages/pokedex_page.dart';
import 'src/ui/widgets/themes/app_theme.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: AppThemes.lightTheme,
      title: 'POKEDEX',
      debugShowCheckedModeBanner: false,
      home: const Scaffold(
        body: SafeArea(child: PokedexPage()),
      ),
    );
  }
}
