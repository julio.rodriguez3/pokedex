import 'package:pokedex/src/ui/responsive/responsive.dart';

import '../../../models/pokemon_data.dart';
import '../molecules/data_information_card_molecule.dart';
import '../organisms/footer_organism.dart';
import '../organisms/header_organism.dart';
import '../organisms/main_organism.dart';

export '../molecules/data_information_card_molecule.dart';

class PokedexTemplate extends StatelessWidget {
  final PokemonData pokemonData;
  const PokedexTemplate({Key? key, required this.pokemonData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    const pokemon = PokemonData(
        name: "Pikachu",
        imagePatch: "assets/pokemons_images/charizard.png",
        no: "no",
        level: "level",
        type: "type",
        hability: "hability",
        height: "height",
        weight: "weight");
    final isMobil = MediaQuery.of(context).isMobil;
    return SingleChildScrollView(
      child: SizedBox(
        height: 800,
        child: Column(
          children: [
            const HeaderOrganism(),
            Flexible(
                flex: 4,
                child: MainOrganism(
                  pokemonData: pokemonData,
                )),
            Flexible(
              flex: isMobil ? 2 : 1,
              child: const FooterOrganism(
                pokemons: [
                  pokemon,
                  pokemon,
                  pokemon,
                  pokemon,
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
