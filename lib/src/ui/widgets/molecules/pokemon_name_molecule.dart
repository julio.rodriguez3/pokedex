import '../atoms/image_atom.dart';
import '../atoms/text_atom.dart';

const double _typeImageHeight = 10;
const double _typeImageWidth = 10;
const _pokemonNameColor = Colors.redAccent;

class PokemonName extends StatelessWidget {
  final String typePokemonImage;
  final String pokemonName;
  const PokemonName(
      {Key? key, required this.typePokemonImage, required this.pokemonName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: _pokemonNameColor, borderRadius: BorderRadius.circular(20)),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ImageAtom(
              imagePatch: typePokemonImage,
              height: _typeImageHeight,
              width: _typeImageWidth),
          TextAtom(text: pokemonName)
        ],
      ),
    );
  }
}
