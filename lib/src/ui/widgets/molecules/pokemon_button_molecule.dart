import '../atoms/button_atom.dart';
import '../atoms/image_atom.dart';

class PokemonButtonMolecule extends StatelessWidget {
  final String? imagePatch;
  const PokemonButtonMolecule({Key? key, required this.imagePatch})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonAtom(width: 90, child: ImageAtom(imagePatch: imagePatch));
  }
}
