

import '../atoms/text_atom.dart';

class PokemonDataItem extends StatelessWidget {
  final String name;
  final String value;
  const PokemonDataItem({Key? key,required this.name,required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context).textTheme;
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextAtom(text: name,textStyle: TextStyle(fontSize: theme.bodyText2?.fontSize),textAlign: TextAlign.left,),
        TextAtom(text: value,textStyle: TextStyle(fontSize: theme.bodyText2?.fontSize),textAlign: TextAlign.left,)
      ],
    );
  }
}
