import 'package:flutter/material.dart';

import '../../../models/pokemon_data.dart';
import 'pokemon_data_item_molecule.dart';
import '../tokens/decorations.dart';

export 'package:flutter/material.dart';

class DataInformationCardMolecule extends StatelessWidget {
  final PokemonData pokemonData;
  const DataInformationCardMolecule({Key? key, required this.pokemonData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      alignment: Alignment.center,
      decoration: dataInformationCardDecoration,
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          PokemonDataItem(name: 'NO.', value: pokemonData.no),
          PokemonDataItem(name: 'LEVEL', value: pokemonData.level),
          PokemonDataItem(name: 'TYPE', value: pokemonData.type),
          PokemonDataItem(name: 'HABILITY', value: pokemonData.hability),
          PokemonDataItem(name: 'HEIGHT', value: pokemonData.height),
          PokemonDataItem(name: 'WEIGHT', value: pokemonData.weight),
        ],
      ),
    );
  }
}
