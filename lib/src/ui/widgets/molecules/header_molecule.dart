import '../atoms/image_atom.dart';
import '../atoms/text_atom.dart';

class HeaderMolecule extends StatelessWidget {
  final String patchImage;
  final String text;
  const HeaderMolecule({Key? key, required this.patchImage, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        ImageAtom(
          imagePatch: patchImage,
          width: 20,
        ),
        TextAtom(text: text)
      ],
    );
  }
}
