import 'package:flutter/material.dart';
import 'package:pokedex/src/ui/responsive/responsive.dart';

import '../molecules/header_molecule.dart';

class HeaderOrganism extends StatelessWidget {
  const HeaderOrganism({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobil = MediaQuery.of(context).isMobil;
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        width: double.infinity,
        alignment: isMobil ? Alignment.center : Alignment.centerLeft,
        decoration: const BoxDecoration(color: Colors.blue),
        child: const HeaderMolecule(
          patchImage: "assets/pokeball.png",
          text: "POKÉDEX_",
        ));
  }
}
