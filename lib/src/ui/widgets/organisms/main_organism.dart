import '../../../models/pokemon_data.dart';
import '../atoms/image_atom.dart';
import '../molecules/data_information_card_molecule.dart';
import '../molecules/pokemon_name_molecule.dart';
import '../../responsive/responsive.dart';

class MainOrganism extends StatelessWidget {
  final PokemonData pokemonData;
  const MainOrganism({Key? key, required this.pokemonData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobil = MediaQuery.of(context).isMobil;

    return Container(
      color: Colors.grey,
      padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 20),
      child: isMobil
          ? _MobilMainOrganism(pokemonData: pokemonData)
          : _DesktopMainOrganism(pokemonData: pokemonData),
    );
  }
}

class _DesktopMainOrganism extends StatelessWidget {
  const _DesktopMainOrganism({Key? key, required this.pokemonData})
      : super(key: key);
  final PokemonData pokemonData;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              PokemonName(
                  typePokemonImage: "assets/flama.png",
                  pokemonName: pokemonData.name),
              Expanded(child: ImageAtom(imagePatch: pokemonData.imagePatch))
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: DataInformationCardMolecule(
            pokemonData: pokemonData,
          ),
        )
      ],
    );
  }
}

class _MobilMainOrganism extends StatelessWidget {
  const _MobilMainOrganism({
    Key? key,
    required this.pokemonData,
  }) : super(key: key);

  final PokemonData pokemonData;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Positioned(
            top: 0,
            child: PokemonName(
                typePokemonImage: "assets/flama.png",
                pokemonName: pokemonData.name)),
        Positioned(
            top: 150,
            child: DataInformationCardMolecule(
              pokemonData: pokemonData,
            )),
        Positioned(
          top: 10,
          child: ImageAtom(imagePatch: pokemonData.imagePatch, width: 200),
        ),
      ],
    );
  }
}
