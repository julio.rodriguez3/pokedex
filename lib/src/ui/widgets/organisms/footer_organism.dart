import 'package:pokedex/src/ui/responsive/responsive.dart';

import '../../../models/pokemon_data.dart';
import '../atoms/text_atom.dart';
import '../molecules/pokemon_button_molecule.dart';

class FooterOrganism extends StatelessWidget {
  final List<PokemonData> pokemons;
  const FooterOrganism({Key? key, required this.pokemons}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobil = MediaQuery.of(context).isMobil;
    return SizedBox(
      width: double.infinity,
      child: isMobil
          ? _MobilFooterOrganism(pokemons: pokemons)
          : _DesktopFooterOrganism(pokemons: pokemons),
    );
  }
}

class _DesktopFooterOrganism extends StatelessWidget {
  const _DesktopFooterOrganism({Key? key, required this.pokemons})
      : super(key: key);
  final List<PokemonData> pokemons;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const TextAtom(text: "OTHER", textAlign: TextAlign.center),
        if (pokemons.length > 1)
          PokemonButtonMolecule(
            imagePatch: pokemons[0].imagePatch,
          ),
        PokemonButtonMolecule(
          imagePatch: pokemons[1].imagePatch,
        ),
        if (pokemons.length > 3)
          PokemonButtonMolecule(
            imagePatch: pokemons[2].imagePatch,
          ),
        PokemonButtonMolecule(
          imagePatch: pokemons[3].imagePatch,
        ),
      ],
    );
  }
}

class _MobilFooterOrganism extends StatelessWidget {
  const _MobilFooterOrganism({
    Key? key,
    required this.pokemons,
  }) : super(key: key);

  final List<PokemonData> pokemons;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const TextAtom(text: "OTHER", textAlign: TextAlign.center),
        if (pokemons.length > 1)
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              PokemonButtonMolecule(
                imagePatch: pokemons[0].imagePatch,
              ),
              PokemonButtonMolecule(
                imagePatch: pokemons[1].imagePatch,
              ),
            ],
          ),
        if (pokemons.length > 3)
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              PokemonButtonMolecule(
                imagePatch: pokemons[2].imagePatch,
              ),
              PokemonButtonMolecule(
                imagePatch: pokemons[3].imagePatch,
              ),
            ],
          ),
      ],
    );
  }
}
