import 'package:flutter/material.dart';

const double _paddingValueOnEnter = 10;
const double _paddingValueOnExit = 20;

class ButtonAtom extends StatefulWidget {
  final Widget child;
  final double? width;
  final double? height;
  const ButtonAtom({Key? key, required this.child, this.width, this.height})
      : super(key: key);

  @override
  State<ButtonAtom> createState() => _ButtonAtomState();
}

class _ButtonAtomState extends State<ButtonAtom> {
  double _paddingValue = _paddingValueOnExit;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
        onEnter: (event) {
          _paddingValue = _paddingValueOnEnter;
          setState(() {});
        },
        onExit: (event) {
          _paddingValue = _paddingValueOnExit;
          setState(() {});
        },
        child: AnimatedContainer(
          margin: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(20)),
          width: widget.width,
          height: widget.height,
          padding: EdgeInsets.all(_paddingValue),
          duration: const Duration(milliseconds: 500),
          child: widget.child,
        ));
  }
}
