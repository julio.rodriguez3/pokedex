import 'package:flutter/material.dart';
export  'package:flutter/material.dart';

class ImageAtom extends StatelessWidget {
  final String? imagePatch;
  final double? height;
  final double? width;
  const ImageAtom({Key? key,required this.imagePatch, this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return imagePatch==null?Container(): Image.asset(imagePatch!,width: width,height: height,);
  }
}
