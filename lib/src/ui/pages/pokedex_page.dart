import '../../models/pokemon_data.dart';
import '../widgets/templates/pokedex_template.dart';

class PokedexPage extends StatelessWidget {
  const PokedexPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const PokedexTemplate(
        pokemonData: PokemonData(
            name: "CHARIZARD",
            no: '006',
            level: '100',
            type: 'FIRE',
            hability: 'FLAME',
            height: '1,7 m',
            weight: '90,5 Kg',
            imagePatch: "assets/pokemons_images/charizard.png"));
  }
}
