

class PokemonData {
  final String name;
  final String no;
  final String level;
  final String type;
  final String hability;
  final String height;
  final String weight;
  final String imagePatch;
  const PokemonData({required this.name,required this.imagePatch,
    required this.no,required this.level,required this.type,required this.hability,required this.height,required this.weight,
  });
}